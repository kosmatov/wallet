# frozen_string_literal: true

require 'pry'
require 'vcr'
require 'wallet'

Wallet.config.key_file = 'spec/test_key'
Wallet.config.confirmation = false

VCR.configure do |c|
  c.hook_into :webmock
  c.cassette_library_dir = 'spec/cassettes'
  c.configure_rspec_metadata!

  default_options = { re_record_interval: false }
  default_options[:record] = :new_episodes
  default_options[:record] = :none if ENV.key?('CI')

  c.default_cassette_options = default_options
end

def with_real_key_file
  default_key_file = Wallet.config.key_file

  Wallet.config.key_file = 'spec/bitcoin-testnet-key'
  yield
ensure
  Wallet.config.key_file = default_key_file
end

def real_dest_addr
  'mv4rnyY3Su5gjcDNzbMLKBQkBicCtHUtFB'
end
