# frozen_string_literal: true

RSpec.describe Wallet::Key do
  let(:key_file) { Wallet.config.key_file }

  describe '.generate' do
    it 'generates private key' do
      key = described_class.generate
      expect(key.pub).to be
    end
  end

  describe '.load' do
    context 'when key file exists' do
      before do
        described_class.generate.save
      end

      after do
        File.unlink(key_file)
      end

      it 'load key from file' do
        key = described_class.load
        expect(key.pub.length).to be > 0
        expect(key.priv.length).to be > 0
      end
    end

    context 'when key file not exists' do
      it 'raises error' do
        expect { described_class.load }.to raise_error(Wallet::Key::FileNotFoundError)
      end
    end
  end

  describe '#save' do
    after do
      File.unlink(key_file)
    end

    it 'saves key to file' do
      expect { described_class.generate.save }.to change {
        File.exist?(key_file)
      }.from(false).to(true)
    end

    context 'when file exists' do
      before do
        described_class.generate.save
      end

      it 'raises error' do
        expect { described_class.generate.save }.to raise_error(Wallet::Key::FileExistsError)
      end
    end
  end
end
