# frozen_string_literal: true

RSpec.describe Wallet::Transaction, :vcr do
  let(:destaddr) { real_dest_addr }

  let(:transaction) do
    with_real_key_file do
      described_class.new(Wallet::Key.load, 0.001, destaddr)
    end
  end

  describe '#build' do
    it 'success' do
      tx = transaction.build

      expect(transaction.fee).to be > 0

      expect(tx.size).to be > 0
      expect(tx.out.size).to eq(2)
      expect(tx.out.first.value).to eq(100_000)
      expect(tx.out.last.value).to be > 0

      tx = Bitcoin::P::Tx.new(tx.to_payload)

      expect(tx.size).to be > 0
      expect(tx.out.size).to eq(2)
      expect(tx.out.first.value).to eq(100_000)
      expect(tx.out.last.value).to be > 0
    end

    context 'with insufficient funds' do
      let(:transaction) do
        with_real_key_file do
          described_class.new(Wallet::Key.load, 1, destaddr)
        end
      end

      it 'failed' do
        expect { transaction.build }.to raise_error(Wallet::Transaction::InsufficientFundsError)
      end
    end
  end

  describe '#to_hex' do
    it 'generates valid hex payload' do
      tx = transaction.build
      hex = transaction.to_hex

      ctrl_tx = Bitcoin::P::Tx.new(hex.split.pack('H*'))

      expect(tx.to_payload).to eq(ctrl_tx.to_payload)
    end
  end

  describe '#satoshis' do
    it 'returns amount in satoshis' do
      expect(transaction.satoshis).to eq(100_000)
    end
  end

  describe '#fee_btc' do
    it 'returns tx fee in BTC' do
      transaction.build

      expect(transaction.fee_btc).to be > 0
    end
  end

  describe '#push' do
    it 'push transaction' do
      transaction.build
      result = transaction.push

      expect(result).to be
    end
  end
end
