# frozen_string_literal: true

RSpec.describe Wallet::Cli::Commands::Key::Generate do
  describe '#call' do
    after do
      File.unlink(Wallet.config.key_file)
    end

    it 'generates private key' do
      expect { subject.call }.not_to raise_error
      expect(File.exist?(Wallet.config.key_file)).to be true
    end
  end
end
