# frozen_string_literal: true

RSpec.describe Wallet::Cli::Commands::Send, :vcr do
  let(:address) { real_dest_addr }

  describe '#call' do
    it 'success' do
      with_real_key_file do
        expect { subject.call(address: address, amount: 0.001) }.not_to raise_error
      end
    end

    context 'when amount greather than balance' do
      it 'shows error message' do
        with_real_key_file do
          expect { subject.call(address: address, amount: 1.001) }.not_to raise_error
        end
      end
    end
  end

  describe '#summary_message' do
    let(:transaction) do
      with_real_key_file do
        Wallet::Transaction.new(subject.key, 0.001, address)
      end
    end

    it 'returns summary' do
      transaction.build

      message = subject.summary_message(transaction)
      amount = subject.estimated_balance(transaction)
      expect(message).to include("remaining balance: #{amount}")
    end
  end
end
