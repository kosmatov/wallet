# frozen_string_literal: true

RSpec.describe Wallet::Cli::Commands::Version do
  describe '#call' do
    it 'success' do
      expect { subject.call }.not_to raise_error
    end
  end
end
