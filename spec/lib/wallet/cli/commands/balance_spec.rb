# frozen_string_literal: true

RSpec.describe Wallet::Cli::Commands::Balance, :vcr do
  describe '#call' do
    it 'success' do
      with_real_key_file do
        expect { subject.call }.not_to raise_error
      end
    end
  end
end
