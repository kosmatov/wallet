# frozen_string_literal: true

RSpec.describe Wallet::Balance, :vcr do
  let(:balance) do
    with_real_key_file do
      described_class.new(Wallet::Key.load)
    end
  end

  describe '#amount' do
    it 'shows balance amount' do
      expect(balance.amount).to be > 0
    end
  end

  describe '#amount_btc' do
    it 'shows balance amount in BTC' do
      expect(balance.amount_btc).to be_a(BigDecimal)
      expect(balance.amount_btc).to be > 0
    end
  end
end
