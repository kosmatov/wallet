# wallet
Sample CLI BTC wallet

## By docker-compose

### Setup
```
$ make bundle
```

### Run
```
$ make console
root@docker:/app# bin/wallet help
```

### Test
```
$ make test
```


Test wallet address: mzdXYrb8JcSvwFXoDKjNHcmXdGGnK2a6aV
