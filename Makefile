CONSOLE=docker-compose run --rm console

bundle:
	$(CONSOLE) bundle

test:
	$(CONSOLE) bundle exec rspec

console:
	$(CONSOLE)
