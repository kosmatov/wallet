# frozen_string_literal: true

require 'bitcoin'
require 'dry-configurable'
require 'delegate'
require 'faraday'
require 'oj'
require 'multi_json'

Bitcoin.network = :testnet3

module Wallet
  extend Dry::Configurable

  VERSION = '0.0.1'
  SATOSHI_IN_BTC = 100_000_000

  autoload :Cli, 'wallet/cli'
  autoload :Key, 'wallet/key'
  autoload :Balance, 'wallet/balance'
  autoload :Transaction, 'wallet/transaction'
  autoload :Blockstream, 'wallet/blockstream/client'

  setting :key_file, default: 'bitcoin-testnet-key'
  setting :bs_base_url, default: 'https://blockstream.info/testnet/api'
  setting :confirmation, default: true
end
