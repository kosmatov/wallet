# frozen_string_literal: true

module Wallet
  class Key < SimpleDelegator
    FileExistsError = Class.new(StandardError)
    FileNotFoundError = Class.new(StandardError)

    class << self
      def generate
        new(Bitcoin::Key.generate)
      end

      def load
        key_file = Wallet.config.key_file
        raise FileNotFoundError, key_file unless File.exist?(key_file)

        keys = File.readlines(key_file, chomp: true)
        bkey = Bitcoin::Key.new(*keys[0..1])
        new(bkey)
      end
    end

    def save
      key_file = Wallet.config.key_file
      raise FileExistsError, key_file if File.exist?(key_file)

      File.write(Wallet.config.key_file, "#{priv}\n#{pub}")
    end
  end
end
