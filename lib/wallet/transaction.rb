# frozen_string_literal: true

module Wallet
  class Transaction
    include Bitcoin::Builder

    InsufficientFundsError = Class.new(StandardError)

    attr_reader :key, :balance, :bs_client, :fee, :amount, :dest_address

    def initialize(key, amount, address)
      @key = key
      @amount = BigDecimal(amount.to_s)
      @dest_address = address

      @balance = Balance.new(key)
      @bs_client = Blockstream::Client.new
    end

    def build
      base_tx = build_btx
      @fee = base_tx.calculate_minimum_fee.to_i
      raise InsufficientFundsError if remaining_amount.negative?

      @tx = build_btx
    end

    def push
      bs_client.push_tx(to_hex)
    end

    def satoshis
      (amount * SATOSHI_IN_BTC).to_i
    end

    def to_hex
      payload = @tx&.to_payload
      return if payload.nil?

      payload.to_s.unpack1('H*')
    end

    def fee_btc
      BigDecimal(fee.to_i.to_s) / SATOSHI_IN_BTC
    end

    def remaining_amount
      balance.amount - satoshis - fee.to_i
    end

    private

    def build_btx
      build_tx do |t|
        t.input do |inp|
          input_txs.each { |(itx, i)| inp.prev_out(itx, i) }
          inp.signature_key key
        end

        t.output do |out|
          out.value satoshis
          out.to dest_address
        end

        t.output do |out|
          out.value remaining_amount
          out.to key.addr
        end
      end
    end

    def utxo
      bs_client.utxo(key)
    end

    def input_txs
      @input_txs ||= utxo.flat_map do |entry|
        data = bs_client.tx_raw(entry['txid'])
        tx = Bitcoin::P::Tx.new(data)

        data = bs_client.tx(entry['txid'])
        data['vout'].filter_map.with_index do |e, i|
          next if e['scriptpubkey_address'] != key.addr

          [tx, i]
        end
      end
    end
  end
end
