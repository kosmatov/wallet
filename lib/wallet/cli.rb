# frozen_string_literal: true

require 'dry/cli'

module Wallet
  module Cli
    autoload :Commands, 'wallet/cli/commands'
  end
end
