# frozen_string_literal: true

module Wallet
  class Balance
    DataFetchError = Class.new(StandardError)

    attr_reader :key, :bs_client

    def initialize(key)
      @key = key
      @bs_client = Blockstream::Client.new
    end

    def amount
      @amount ||= funded_sum - spent_sum
    end

    def amount_btc
      BigDecimal(amount) / SATOSHI_IN_BTC
    end

    private

    def spent_sum
      chain_stats['spent_txo_sum'] + mempool_stats['spent_txo_sum']
    end

    def funded_sum
      chain_stats['funded_txo_sum'] + mempool_stats['funded_txo_sum']
    end

    def chain_stats
      address_data['chain_stats']
    end

    def mempool_stats
      address_data['mempool_stats']
    end

    def address_data
      @address_data ||= fetch_address_data
    end

    def fetch_address_data
      bs_client.address(key)
    rescue Blockstream::RequestError
      raise DataFetchError
    end
  end
end
