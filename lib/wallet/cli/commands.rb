# frozen_string_literal: true

require_relative 'commands/version'
require_relative 'commands/balance'
require_relative 'commands/address'
require_relative 'commands/key/generate'
require_relative 'commands/send'

module Wallet
  module Cli
    module Commands
      extend Dry::CLI::Registry

      register 'version', Version, aliases: ['-v', '--version']
      register 'balance', Balance
      register 'address', Address
      register 'send', Send
      register 'key' do |prefix|
        prefix.register 'generate', Key::Generate
      end
    end
  end
end
