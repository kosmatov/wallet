# frozen_string_literal: true

module Wallet
  module Cli
    module Commands
      class Version < Dry::CLI::Command
        desc 'print version'

        def call(*)
          puts Wallet::VERSION
        end
      end
    end
  end
end
