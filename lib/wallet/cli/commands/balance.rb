# frozen_string_literal: true

module Wallet
  module Cli
    module Commands
      class Balance < Dry::CLI::Command
        desc 'show balance'

        def call(*)
          balance = Wallet::Balance.new(Wallet::Key.load)

          puts "#{balance.amount_btc.to_f} BTC"
        rescue Wallet::Key::FileNotFoundError
          puts "key file \"#{Wallet.config.key_file}\" not exists"
        rescue Wallet::Balance::DataFetchError
          puts 'error fetch data from blockchain'
        end
      end
    end
  end
end
