# frozen_string_literal: true

module Wallet
  module Cli
    module Commands
      class Send < Dry::CLI::Command
        desc 'send BTC to another wallet'

        argument :address, type: :string, required: true, desc: 'destination wallet address'
        argument :amount, type: :float, required: true, desc: 'amount in BTC'

        def call(address:, amount:, **)
          transaction = Wallet::Transaction.new(key, amount, address)
          transaction.build

          ask_confirmation(transaction) if Wallet.config.confirmation

          txid = transaction.push
          puts "\ntransaction ID: #{txid}"
        rescue Wallet::Key::FileNotFoundError
          puts "key file \"#{Wallet.config.key_file}\" not exists"
        rescue Wallet::Balance::DataFetchError
          puts 'error fetch balance data from blockchain'
        rescue Wallet::Transaction::InsufficientFundsError
          puts 'insufficient balance to make transaction'
          puts summary_message(transaction)
        end

        def key
          @key ||= Wallet::Key.load
        end

        def balance
          @balance ||= Wallet::Balance.new(key)
        end

        def ask_confirmation(transaction)
          puts "new transfer to #{transaction.dest_address}"
          puts summary_message(transaction)
          print 'confirm? [y/N] '

          answer = $stdin.gets.chomp
          exit unless answer =~ /\Ay(e[sp]?)?\z/i
        end

        def summary_message(transaction)
          <<~MSG

            current balance: #{balance.amount_btc.to_f} BTC
            amount to send: #{transaction.amount.to_f} BTC
            network fee: #{transaction.fee_btc.to_f} BTC
            remaining balance: #{estimated_balance(transaction)} BTC

          MSG
        end

        def estimated_balance(transaction)
          amount = BigDecimal(transaction.remaining_amount.to_s)
          (amount / Wallet::SATOSHI_IN_BTC).to_f
        end
      end
    end
  end
end
