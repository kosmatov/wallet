# frozen_string_literal: true

module Wallet
  module Cli
    module Commands
      class Address < Dry::CLI::Command
        desc 'show wallet address'

        def call(*)
          puts Wallet::Key.load.addr
        rescue Wallet::Key::FileNotFoundError
          puts "key file \"#{Wallet.config.key_file}\" not found"
        end
      end
    end
  end
end
