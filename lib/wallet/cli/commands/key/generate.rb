# frozen_string_literal: true

module Wallet
  module Cli
    module Commands
      module Key
        class Generate < Dry::CLI::Command
          desc 'generate private key'

          def call(*)
            Wallet::Key.generate.save

            puts "new key saved to \"#{Wallet.config.key_file}\""
          rescue Wallet::Key::FileExistsError
            puts "key file \"#{Wallet.config.key_file}\" already exists"
          end
        end
      end
    end
  end
end
