# frozen_string_literal: true

module Wallet
  module Blockstream
    RequestError = Class.new(StandardError)

    class Client
      attr_reader :base_url, :key

      def initialize
        @base_url = Wallet.config.bs_base_url
      end

      def address(key)
        get("/address/#{key.addr}")
      end

      def utxo(key)
        get("/address/#{key.addr}/utxo")
      end

      def tx(txid)
        get("/tx/#{txid}")
      end

      def tx_raw(txid)
        get("/tx/#{txid}/raw", format: :raw)
      end

      def push_tx(tx_hex)
        post('/tx', tx_hex)
      end

      private

      def get(path, format: :json)
        response = Faraday.get("#{base_url}#{path}")
        raise RequestError, response.body unless response.success?

        return MultiJson.load(response.body) if format == :json

        response.body
      end

      def post(path, body)
        response = Faraday.post("#{base_url}#{path}", body, 'Content-Type' => 'text/plain')
        raise RequestError, response.body unless response.success?

        response.body
      end
    end
  end
end
